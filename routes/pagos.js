/*
* ADRIAN JOSE GOSALVEZ MACIA
* 48620879-Y
*/
var express 	= require('express')
var pagos 		= express.Router()
module.exports 	= pagos

require('../helpers/tools')()
var connection = connect()

/**
  * Obtiene listado completo de pagos
  * @name Obtener_todos_los_pagos
  * @example GET /pagos
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
pagos.get('/', checkAuth, function(pet, resp){ 
	totalRows('pago', function(err, total){
        if (err) {
            console.log("ERROR : ",err); 
            resp.status(500).send("Error en el servidor").end()           
		} else {            
            var query = 'SELECT * FROM pago'
			var subQuery = paginacionQuery(pet, total)
			console.log(query + subQuery)
			connection.query(query + subQuery, function(err, rows, fields){
				if (!err) {
					if (rows.length == 0) {
						resp.status(404).send("Objeto no encontrado").end()
					} else {
						var hm = hipermedia("pagos", null, total)
						var pages = paginacion("pagos", pet.query.pagina, total)
						var basic = {"status-basic:":"authenticated"}
						var res = [rows, pages, hm, basic] 
						resp.status(200).send(res).end()
					}
				} else {
					console.log("Error 500")
					resp.status(500).send("Server error").end()
				}
			}) 
        }
	})
})

/**
  * Obtiene un único pago
  * @name Obtener_un_pago
  * @param {id} Identificador de pago
  * @example GET /pagos/1
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
pagos.get('/:id', checkAuth, function(pet, resp){
	var idPago = parseInt(pet.params.id)
	var query = 'SELECT * FROM pago WHERE id = ' + idPago
	connection.query(query, function(err, rows, fields){
		if (isNaN(idPago)) {
			resp.status(400).send("Objeto no encontrado").end();
		} else {
			if (!err) {
				if (rows.length == 0) {
					resp.status(404).send("Objeto no encontrado").end()
				} else {
					var hm = hipermedia("pagosid", idPago)
					var basic = {"status-basic:":"authenticated"}
					var res = [rows, hm, basic] 
					console.log("Get pagos/" + idPago + " ok!")
					resp.status(200).send(res).end()
				}
			} else {
				console.log("Error en la consulta")
				resp.status(500).send("Error interno").end()
			}
		}
	}) 
})

/**
  * Graba un pago nuevo
  * @name Nuevo_pago
  * @example POST /pagos {'idCliente':1,'idPresupuesto':2,'nuTarjeta':'12548987465486540','cad':'12/21', 'codigo':'541'}
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
pagos.post('/', checkAuth, function(pet, resp){
	var nuevo 			= pet.body
	var idCliente 		= nuevo.idCliente
	var idPresupuesto 	= nuevo.idPresupuesto
	var numTarjeta 		= nuevo.numTarjeta
	var cad 			= nuevo.cad
	var codigo 			= nuevo.codigo
	var idServicio		= -1
	var fechaEvento		= ''

	var query_select_presupuesto = 'SELECT idServicio, fechaEvento FROM presupuesto WHERE id = ' + idPresupuesto

	connection.query(query_select_presupuesto, function(err, rows, fields){
		if (!err) {
			var obj 		= JSON.parse(JSON.stringify(rows))
			idServicio 		= obj[0]["idServicio"]
			fechaEvento 	= obj[0]["fechaEvento"]

			var query = 'INSERT INTO pago (idCliente, idPresupuesto, numTarjeta, cad, codigo, pagado) VALUES (' + idCliente + ', ' + idPresupuesto + ', "' + numTarjeta + '", "' + cad + '", "' + codigo + '", 1)'
			connection.query(query, function(err2, rows2, fields2){
				if (!err) {
					if (rows2["affectedRows"] == 1) {
						console.log("OK: " + query)
						var query_insert_agenda = 'INSERT INTO agenda (fecha, idServicio) VALUES ("' + fechaEvento + '", "' + idServicio + '")'
						connection.query(query_insert_agenda, function(err3, rows3, fields3){
							if (!err3) {
								resp.status(201).send("ok").end()
							} else {
								console.log("ERROR3: " + query_insert_agenda)
								resp.status(400).send("request error").end()
							}
						})
					} else {
						console.log("ERROR: " + query)
						resp.status(400).send("request error").end()
					}
				} else {
						console.log("ERROR2: " + query)
						resp.status(500).send("server error").end()
				}
			}) 
		} else {
			console.log("ERROR4: " + query_select_presupuesto)
			resp.status(500).send("server error").end()
		}
	})
})