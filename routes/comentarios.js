/*
* ADRIAN JOSE GOSALVEZ MACIA
* 48620879-Y
*/
var express 	= require('express')
var comentarios	= express.Router()
module.exports 	= comentarios

require('../helpers/tools')()
var connection = connect()

/**
  * Obtiene listado completo de comentarios
  * @name Obtener_todos_los_comentarios
  * @example GET /comentarios
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
comentarios.get('/', checkAuth, function(pet, resp){ 
	totalRows('comentario_nxn', function(err, total){
        if (err) {
            console.log("ERROR : ",err); 
            resp.status(500).send("Error en el servidor").end()           
		} else {            
            var query = 'SELECT * FROM comentario_nxn'
			var subQuery = paginacionQuery(pet, total)
			console.log(query + subQuery)
			connection.query(query + subQuery, function(err, rows, fields){
				if (!err) {
					if (rows.length == 0) {
						resp.status(404).send("Objeto no encontrado").end()
					} else {
						var hm = hipermedia("comentarios", null, total)
						var pages = paginacion("comentario", pet.query.pagina, total)
						var basic = {"status-basic:":"authenticated"}
						var res = [rows, pages, hm, basic] 
						resp.status(200).send(res).end()
					}
				} else {
					console.log("Error 500")
					resp.status(500).send("Server error").end()
				}
			}) 
        }
	})
})

/**
  * Obtiene un único comentario
  * @name Obtener_un_comentario
  * @param {id} Identificador de comentario
  * @example GET /comentarios/1
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
comentarios.get('/:id', checkAuth, function(pet, resp){
	var idComentario = parseInt(pet.params.id)
	var query = 'SELECT * FROM comentario_nxn WHERE id = ' + idComentario
	connection.query(query, function(err, rows, fields){
		if (!err) {
			if (rows.length == 0) {
				resp.status(404)
				resp.send("Objeto no encontrado")
				resp.end()
			} else {
				var hm = hipermedia("comentariosid", idComentario)
				var basic = {"status-basic:":"authenticated"}
				var res = [rows, hm, basic] 
				console.log("Get comentarios/ ok!")
				resp.status(200).send(res).end()
			}
		} else {
			console.log("Error en la consulta")
			resp.status(404)
			resp.send("Objeto no encontrado")
			resp.end()
		}
	}) 
})

/**
  * Graba un comentario nuevo
  * @name Nuevo_comentario
  * @example POST /comentarios {'idServicio':2,'idCliente':1,'comentario':'Esto es un nuevo comentario'}
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
comentarios.post('/', checkAuth, function(pet, resp){
	var nuevo 		= pet.body
	var idServicio 	= nuevo.idServicio
	var idCliente 	= nuevo.idCliente
	var comentario 	= nuevo.comentario

	var query = 'INSERT INTO comentario_nxn (idServicio, idCliente, comentario)  VALUES (' + idServicio + ', ' + idCliente + ', "' + comentario + '")'
	connection.query(query, function(err, rows, fields){
		if (!err) {
			if (rows["affectedRows"] == 1) {
				console.log("OK: " + query)
				resp.header('Location','comentario/' + rows["insertId"]) 
				resp.status(201).send("ok").end()
			} else {
				console.log("ERROR: " + query)
				resp.status(400)
				resp.send("request error")
				resp.end()
			}
		} else {
				console.log("ERROR2: " + query)
				resp.status(500)
				resp.send("server error")
				resp.end()
		}
	}) 
})