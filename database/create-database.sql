-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 13-01-2017 a las 14:59:49
-- Versión del servidor: 5.7.16-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `adi_api`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agenda`
--

CREATE TABLE `agenda` (
  `id` int(11) NOT NULL,
  `fecha` varchar(255) NOT NULL,
  `idServicio` int(11) NOT NULL,
  `detalle` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `agenda`
--

INSERT INTO `agenda` (`id`, `fecha`, `idServicio`, `detalle`) VALUES
(1, '2016-06-05', 3, ''),
(2, '2016-10-09', 0, 'Fiesta Com. Valenciana'),
(3, '2016-10-12', 0, 'Fiesta nacional'),
(4, '2016-12-24', 0, 'Noche buena'),
(5, '2016-12-25', 0, 'Navidad'),
(6, '2016-05-01', 0, 'Dia del trabajador'),
(7, '2016-12-31', 0, 'Nochevieja'),
(8, '2017-01-01', 0, 'Año nuevo'),
(9, '2016-11-21', 2, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `artista`
--

CREATE TABLE `artista` (
  `id` int(11) NOT NULL,
  `nombreArt` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `especialidad` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `artista`
--

INSERT INTO `artista` (`id`, `nombreArt`, `nombre`, `apellidos`, `dob`, `especialidad`, `direccion`, `telefono`) VALUES
(1, 'Magic Adry', 'Adrián', 'Gosálvez', '1990-02-18', 'Magia', 'Avda. Pintor Baeza, 7', '651042877'),
(2, 'Piticon', 'Manolo', 'Álvarez', '1989-05-12', 'Clown', 'Avda. Novelda, 45', '622149956'),
(3, 'Moisés', 'Moisés', 'Abdel', '1979-09-17', 'Monitor', 'Calle del sillón, 5', '652880657'),
(4, 'Manolo', 'Manuel', 'Marco', '1979-09-17', 'Monitor', 'Calle del sillón, 5', '652880657'),
(5, 'Juan Tamariz', 'Juan', 'Tamariz', '1979-09-17', 'Monitor', 'Calle del sillón, 5', '652880657'),
(6, 'David Copperfield', 'David', 'Copperfield', '1979-09-17', 'Monitor', 'Calle del sillón, 5', '652880657'),
(7, 'Marieta la monitoreta', 'Maria', 'López', '1979-09-17', 'Monitor', 'Calle del rollet, 15', '652880657');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `dni` varchar(9) NOT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `famNumerosa` tinyint(1) DEFAULT '0',
  `password` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `nombre`, `apellidos`, `dni`, `direccion`, `telefono`, `dob`, `famNumerosa`, `password`, `token`) VALUES
(47, 'Adrián', 'Gosálvez Maciá', '12345678A', 'mi calle, 2', '654987654', '1999-12-12', 0, '202cb962ac59075b964b07152d234b70', NULL),
(48, 'Juan', 'Pérez López', '12312312h', 'su calle, 2', '654654654', '1992-12-12', 0, '202cb962ac59075b964b07152d234b70', NULL),
(49, 'Pepa', 'Visiesta', '12345678K', 'Moler, 227', '988999888', '1990-01-19', 0, '202cb962ac59075b964b07152d234b70', NULL),
(50, 'test', 'test test', 'usuario', NULL, NULL, NULL, 0, 'e10adc3949ba59abbe56e057f20f883e', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `id` int(11) NOT NULL,
  `idServicio` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `comentario` text NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`id`, `idServicio`, `idCliente`, `comentario`, `fecha`) VALUES
(1, 1, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '2016-10-10 12:20:14'),
(2, 2, 1, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2016-10-10 12:20:22'),
(3, 3, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '2016-10-10 12:22:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario_nxn`
--

CREATE TABLE `comentario_nxn` (
  `id` int(11) NOT NULL,
  `idServicio` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `comentario` text NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentario_nxn`
--

INSERT INTO `comentario_nxn` (`id`, `idServicio`, `idCliente`, `comentario`, `fecha`) VALUES
(1, 1, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '2016-10-10 12:20:14'),
(2, 2, 2, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2016-10-10 12:20:22'),
(3, 3, 3, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2016-10-10 12:20:22'),
(4, 1, 4, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2016-10-10 12:20:22'),
(5, 2, 5, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2016-10-10 12:20:22'),
(6, 3, 6, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2016-10-10 12:20:22'),
(7, 1, 3, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2016-10-10 12:20:22'),
(8, 2, 3, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2016-10-10 12:20:22'),
(9, 3, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '2016-10-10 12:22:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialidad`
--

CREATE TABLE `especialidad` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `especialidad`
--

INSERT INTO `especialidad` (`id`, `nombre`, `descripcion`) VALUES
(14, 'Magia', 'magia a tope'),
(15, 'Clown', 'payasos para niños\r\n'),
(16, 'Magia2', 'magia a tope'),
(17, 'Clown2', 'payasos para niños');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `id` int(11) NOT NULL,
  `numTarjeta` varchar(16) NOT NULL,
  `cad` varchar(5) NOT NULL,
  `codigo` varchar(3) NOT NULL,
  `pagado` tinyint(1) NOT NULL DEFAULT '0',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(15) NOT NULL DEFAULT '0.0.0.0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pago`
--

INSERT INTO `pago` (`id`, `numTarjeta`, `cad`, `codigo`, `pagado`, `fecha`, `ip`) VALUES
(1, '1234567812345678', '10/22', '578', 1, '2016-10-10 14:39:46', '0.0.0.0'),
(2, '8834567812345678', '12/21', '558', 1, '2016-10-10 14:39:46', '0.0.0.0'),
(3, '1234523112345678', '10/25', '123', 1, '2016-10-10 15:08:34', '0.0.0.0'),
(4, '1234654712345678', '11/24', '523', 1, '2016-10-10 15:28:34', '0.0.0.0'),
(5, '1234561234112345', '12/23', '675', 1, '2016-10-10 15:38:34', '0.0.0.0'),
(6, '1234567874714567', '06/22', '855', 1, '2016-10-10 15:58:34', '0.0.0.0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuesto`
--

CREATE TABLE `presupuesto` (
  `id` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `idServicio` int(11) NOT NULL,
  `idPago` int(11) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `fechaEvento` varchar(10) NOT NULL,
  `variosAnfitriones` tinyint(1) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `lugar` varchar(255) NOT NULL,
  `musica` tinyint(1) NOT NULL,
  `tiempo` int(11) NOT NULL,
  `comentario` varchar(255) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `presupuesto`
--

INSERT INTO `presupuesto` (`id`, `idCliente`, `idServicio`, `idPago`, `tipo`, `fechaEvento`, `variosAnfitriones`, `cantidad`, `lugar`, `musica`, `tiempo`, `comentario`, `fecha`) VALUES
(1, 1, 1, 0, 'Boda', '2016-10-28', 0, 2, 'Restaurante', 1, 45, 'Deseamos que sea algo familiar.', '2016-10-10 08:48:52'),
(2, 2, 3, 0, 'Bautizo', '2016-10-05', 1, 3, 'Chalet privado', 0, 180, 'Para entretener a los niños.', '2016-10-10 08:48:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `artista` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id`, `nombre`, `descripcion`, `artista`) VALUES
(1, 'Animación para niños', 'Entretener a los niños durante la boda', 1),
(2, 'Cumpleaños - Payaso', 'Piticón es el payaso más famoso de la costa levantina donde su narizota grandota será la protagonista de todas las risas.', 2),
(3, 'Excursión', 'Un monitor para una excursión es lo ideal, controlará a los niños y les hará juegos divertidos.', 3),
(18, 'Animación 2', 'fañkldjfañlkdj', 1),
(19, 'Cumpleaños 2', 'asñkdfjañksd', 2),
(20, 'Excursion 2', 'asdfa', 3),
(31, 'test', 'testtest', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `artista`
--
ALTER TABLE `artista`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nombreArt` (`nombreArt`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni` (`dni`),
  ADD KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id`,`idServicio`,`idCliente`);

--
-- Indices de la tabla `comentario_nxn`
--
ALTER TABLE `comentario_nxn`
  ADD PRIMARY KEY (`id`,`idServicio`,`idCliente`);

--
-- Indices de la tabla `especialidad`
--
ALTER TABLE `especialidad`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_2` (`nombre`),
  ADD KEY `nombre` (`nombre`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nombre` (`nombre`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `artista`
--
ALTER TABLE `artista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `comentario_nxn`
--
ALTER TABLE `comentario_nxn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `especialidad`
--
ALTER TABLE `especialidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
