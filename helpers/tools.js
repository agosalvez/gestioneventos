/*
* ADRIAN GOSALVEZ MACIA
*/
var db 	= require('mysql')
var jwt	= require('jwt-simple')

module.exports = function() {
	this.maxRows = 3
    this.connect = function() {
    	var result 	= db.createConnection({
				host 		: 'localhost',
				user 		: 'root',
				password 	: 'adrian',
				database	: 'adi_api'
			})
		return result
	}

	this.generateToken = function(cad) {
		var token = ''
		var secret = 'M1cL4v3#314';
		var pass64 = new Buffer(secret).toString('base64')
		var token = jwt.encode(cad, pass64);
		console.log("Enviando token..." + token)

		return token
	}

	this.hipermedia = function(cad, id) {
		var res = {}
		switch(cad) {
			case 'clientes': res = { "link": [{ "rel" : "self", "href" : "/clientes" },{ "rel" : "cliente", "href" : "/clientes/:idCliente" },{ "rel" : "autenticar", "href" : "/clientes/auth" }]}
				break;
			case 'clientesid' : res = { "link": [{ "rel" : "self", "href" : "/clientes/" + id },{ "rel" : "clientes", "href" : "/clientes" },{ "rel" : "autenticar", "href" : "/clientes/auth" }]}
				break;
			case 'servicios': res = { "link": [{ "rel" : "self", "href" : "/servicios" },{ "rel" : "servicio", "href" : "/servicios/:idServicio" },{ "rel" : "artistas", "href" : "/artistas" }]}
				break;
			case 'serviciosid': res = { "link": [{ "rel" : "self", "href" : "/servicios/" + id },{ "rel" : "servicios", "href" : "/servicios" },{ "rel" : "artistas", "href" : "/artistas" }]}
				break;
			case 'artistas': res = { "link": [{ "rel" : "self", "href" : "/artistas" },{ "rel" : "artista", "href" : "/artistas/:idArtista" },{ "rel" : "servicios", "href" : "/servicios" }]}
				break;
			case 'artistasid' : res = { "link": [{ "rel" : "self", "href" : "/artistas/" + id },{ "rel" : "artistas", "href" : "/artistas" },{ "rel" : "servicios", "href" : "/servicios" }]}
				break;
			case 'agenda' : res = { "link": [{ "rel" : "self", "href" : "/agenda" },{ "rel" : "artistas", "href" : "/artistas" },{ "rel" : "servicios", "href" : "/servicios" }]}
				break;
			case 'comentarios': res = { "link": [{ "rel" : "self", "href" : "/comentarios" },{ "rel" : "comentario", "href" : "/comentarios/:idComentario" },{ "rel" : "servicios", "href" : "/servicios" },{ "rel" : "artistas", "href" : "/artistas" }]}
				break;
			case 'comentariosid' : res = { "link": [{ "rel" : "self", "href" : "/comentarios/" + id },{ "rel" : "comentarios", "href" : "/comentarios" },{ "rel" : "servicios", "href" : "/servicios" },{ "rel" : "artistas", "href" : "/artistas" }]}
				break;
			case 'presupuestos': res = { "link": [{ "rel" : "self", "href" : "/presupuestos" },{ "rel" : "presupuesto", "href" : "/presupuestos/:idPresupuesto" },{ "rel" : "clientes", "href" : "/clientes" },{ "rel" : "servicios", "href" : "/servicios" }]}
				break;
			case 'presupuestosid' : res = { "link": [{ "rel" : "self", "href" : "/presupuestos/" + id },{ "rel" : "presupuestos", "href" : "/presupuestos" },{ "rel" : "clientes", "href" : "/clientes" },{ "rel" : "servicios", "href" : "/servicios" }]}
				break;
			case 'pagos': res = { "link": [{ "rel" : "self", "href" : "/pagos" },{ "rel" : "pago", "href" : "/pagos/:idPago" },{ "rel" : "clientes", "href" : "/clientes" },{ "rel" : "presupuestos", "href" : "/presupuestos" }]}
				break;
			case 'pagosid' : res = { "link": [{ "rel" : "self", "href" : "/pagos/" + id },{ "rel" : "pagos", "href" : "/pagos" },{ "rel" : "clientes", "href" : "/clientes" },{ "rel" : "presupuestos", "href" : "/presupuestos" }]}
				break;
			default:
		}
		return res;
	}
	this.checkAuth = function(pet, res, next) {
		var auth 	= pet.get('authorization')
		var secret 	= 'Basic dXN1YXJpbzoxMjM0NTY='

		if (auth == null) {
			res.status(401).set('WWW-Authenticate', 'Basic realm="myrealm"')
			return res.send("Unauthorized access.")
		}

		if (auth != secret) {
			return res.status(403).send({status:"ok",message:"Data access error. Unauthorized access"})
		}
		next()
	}

	this.paginacionQuery = function(pet, totalRows, maxRowsPet) {
		if (maxRowsPet == null || maxRowsPet == undefined) {
			maxRowsPet = maxRows
		}
		var lastPage = calcularNumPaginas(totalRows, maxRowsPet)
		var pag = pet.query.pagina
		if (pag <= lastPage) {
			if (pag != undefined && pag > 0) {
				subQuery = ' LIMIT ' + ((pag * maxRowsPet) - maxRowsPet) + ', ' + maxRowsPet
			} else {
				subQuery = ''
			}
		} else {
			subQuery = ''
		}
		return subQuery
	}
	this.totalRows = function(table, callback) {
		var query = 'SELECT COUNT(*) as total FROM ' + table
		var max = 0
		connect().query(query, function(err, result){
			if (err) {
	            callback(err,null);
			} else {
	            callback(null,result[0].total);
			}
		})
	}

	this.paginacion = function(obj, pAct, rows, rowsPet) {
		var lastPage = calcularNumPaginas(rows, rowsPet)
		if (pAct > 0 && pAct <= lastPage) {
			var pActual = parseInt(pAct)
			var anterior = 0
			if (pActual > 1) {
				anterior = pActual - 1
			} else {
				anterior = pActual
			}
			var siguiente = pActual + 1
			if (pActual == lastPage) {
				siguiente = pActual
			}

			if (lastPage == 1 || anterior < 1 || siguiente > lastPage) {
				siguiente = 1
			}

			var res = {
				"paginacion" : [
					{
						"rel" : "primera",
						"href" : "/" + obj + "/?pagina=" + 1
					},
					{
						"rel" : "anterior",
						"href" : "/" + obj + "/?pagina=" + anterior
					},
					{
						"rel" : "siguiente",
						"href" : "/" + obj + "/?pagina=" + siguiente
					},
					{
						"rel" : "ultima",
						"href" : "/" + obj + "/?pagina=" + lastPage
					}
				]
			}
			return res
		} else {
			return {"paginacion" : []}
		}
	}

	this.calcularNumPaginas = function(rows, rowsPet) {
		if (rowsPet == null || rowsPet == undefined) {
			maxRowsCalculate = maxRows
		} else {
			maxRowsCalculate = rowsPet
		}
		var paginas = 0
		if ( rows == 0 ) {
			paginas = 1
		} else if ( rows % maxRowsCalculate != 0 ) {
        	paginas = Math.trunc(rows / maxRowsCalculate + 1)
        } else {
        	paginas = Math.trunc(rows / maxRowsCalculate)
        }
        return paginas
	}
}
