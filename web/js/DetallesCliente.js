var React = require('react')

module.exports = React.createClass({
    ocultarDetalles: function () {
      this.props.handleOcultarDetalles(this.props.pos)
    },
    render: function () {
        var famNumerosa = "No"
        if (this.props.famNumerosa) {
          famNumerosa = "Si"
        }
        // DEUDA TECNICA *******************************************************************
        var dob = ''
        if(this.props.dob != null) {
          dob = this.props.dob.substr(0,10)
        }
        // FIN DE DEUDA TECNICA - QUITAR EL IF PORQUE TENDRÁ TODOS LOS CAMPOS **************
        return <div className="detallesItem">
              <br />
              <span className="nombre">{this.props.nombre}</span><br />
              <span className="apellidos">{this.props.apellidos}</span><br />
              <span className="dni">{this.props.dni}</span><br />
              <span className="direccion">{this.props.direccion}</span><br />
              <span className="telefono">{this.props.telefono}</span><br />
              <span className="dob">{dob}</span><br />
              <span className="famNumerosa">{famNumerosa}</span><br />
              <br />
              <a href="javascript:;" onClick={this.ocultarDetalles}>Ocultar detalles</a>
            </div>
    }
})