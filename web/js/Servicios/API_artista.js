
module.exports  = {
	API_URL : 'http://localhost:3000',
    API_URL_ARTISTAS : 'http://localhost:3000/artistas',
    obtenerArtistas: function (direccion, total) {
    	var dir
    	if(direccion == undefined) {
    		dir = "/artistas/?pagina=1"
    	} else {
    		dir = direccion
    	}
        return fetch(this.API_URL + dir + '&total=' + total, {
                    headers: {'Authorization':'Basic dXN1YXJpbzoxMjM0NTY='}
                }).then(function(response) {
                  if (response.ok) {
                      return response.json()
                    }
                })
    }
}
