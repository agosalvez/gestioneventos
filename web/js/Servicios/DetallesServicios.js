var React = require('react')

module.exports = React.createClass({
    ocultarDetalles: function () {
      this.props.handleOcultarDetalles(this.props.pos)
    },
    render: function () {
        return <div className="detalleServicio">
              <h4 className="nombre">{this.props.nombre}</h4>
              <h5 className="descripcion"><b>Descripción:</b> {this.props.descripcion}</h5>
              <h5 className="artista"><b>Artista:</b> {this.props.artista}</h5>
              <a className="btn btn-success" href="javascript:;" onClick={this.ocultarDetalles}>Ocultar detalles</a>
            </div>
    }
})