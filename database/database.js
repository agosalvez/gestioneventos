/*
* ADRIAN GOSALVEZ MACIA
*/
var express 	= require('express')
var database 	= express.Router()
module.exports 	= database

require('../helpers/tools')()
var connection = connect()

/**
  * Obtiene listado completo de clientes
  * @name Resetea_la_base_de_datos
  * @example GET /database/reset
  * @return {respuesta petición}
  */
database.get('/reset', checkAuth, function(pet, resp){
    var query = 'call reset()'
	connection.query(query, function(err, rows){
		if (!err) {
			if (rows.length == 0) {
				resp.status(404).send("Objeto no encontrado").end()
			} else {
				resp.status(200).send("Database reset OK!").end()
			}
		} else {
			console.log("Error 500")
			resp.status(500).send("Server error").end()
		}
	})
})
