var React = require('react')

module.exports = React.createClass({
    ocultarDetalles: function () {
      this.props.handleOcultarDetalles(this.props.pos)
    },
    render: function () {
        return <div className="detalleServicio">
              <h4 className="nombreArt">{this.props.nombreArt}</h4>
              <h5 className="nombre"><b>Nombre:</b> {this.props.nombre}</h5>
              <h5 className="artista"><b>Especialidad:</b> {this.props.especialidad}</h5>
              <h5 className="artista"><b>Telefono:</b> {this.props.telefono}</h5>
              <a className="btn btn-success" href="javascript:;" onClick={this.ocultarDetalles}>Ocultar detalles</a>
            </div>
    }
})