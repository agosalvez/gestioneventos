var React = require('react')
var Servicio = require('./Servicios')
var Artista = require('../Artistas/Artistas')
var DetallesServicio = require('./DetallesServicios')
var EditarServicio = require('./EditarServicios')
var Paginacion = require('./Paginacion')
var API_servicio = require('../servicios/API_servicio')
var API_artista = require('../servicios/API_artista')
//var EventBus = require('./servicios/EventBus')

module.exports = React.createClass({
    componentDidMount: function () {
        //escuchamos el evento 'nuevoCliente' en el bus de eventos
        //si se recibe el evento hay que añadir el item a la lista
        //EventBus.eventEmitter.addListener('nuevoCliente', this.addItem)
        //le pedimos los items al API
        this.cargarServicios(this.state.paginaInicial)
        this.cargarArtistas()
    },
    getInitialState : function () {
      return {servicios : [],
                artistas : [],
                paginaInicial : "/servicios/?pagina=1", 
                queryString : "/servicios/?pagina=", 
                pagina : 1,
                tamPagina : 3}
    },
    verDetalles : function (i) {
        this.setState({detalleServicio: i})
    },
    ocultarDetalles : function () {
        this.setState({detalleServicio: undefined})
    },
    cargarServicios: function (direccion) {
        API_servicio.obtenerServicios(direccion, this.state.tamPagina)
            .then(datos => {
                this.setState({servicios: datos})
            })
    },
    cargarArtistas: function () {
        API_artista.obtenerArtistas()
            .then(datos => {
                this.setState({artistas: datos})
            })
    },
    getArtista: function (pos) {
        for (var i = 0; i < this.state.artistas.length - 3; i++) {
            var lista = this.state.artistas[0]
            var artistaABuscar = undefined
            for (var j = 0; j < lista.length; j++) {
                var actual = lista[j]
                if (lista[j].id == pos) {
                    artistaABuscar = actual
                }
            }
            return artistaABuscar.nombreArt            
        }
    },
    getArtistaId: function (pos) {
        for (var i = 0; i < this.state.artistas.length - 3; i++) {
            var lista = this.state.artistas[0]
            var artistaABuscar = undefined
            for (var j = 0; j < lista.length; j++) {
                var actual = lista[j]
                if (lista[j].id == pos) {
                    artistaABuscar = actual
                }
            }
            return artistaABuscar.id           
        }
    },
    verEditarServicio: function(i) {
        this.setState({editarServicio: i})
    },
    ocultarEditarServicio: function(i) {
        this.setState({editarServicio: undefined})
    },
    editarServicio: function(id, servicio) {
        API_servicio.editarServicio(id, servicio)
            .then(response => {
                if(response.status == "ok"){
                    mensaje(response.message, "success")
                } else {
                    mensaje(response.message, "danger")
                }
                this.cargarServicios(this.state.queryString + this.state.pagina)
            })
    },
    eliminarServicio: function(id) {
        API_servicio.eliminarServicio(id)
            .then(response => {
                if(response.status == "ok"){
                    mensaje(response.message, "success")
                } else {
                    mensaje(response.message, "danger")
                }
                this.cargarServicios(this.state.queryString + 1)
            })
    },
    setTamPagina : function (tamPagina) {
        this.state.tamPagina = tamPagina
        this.pagPrimera()
    },
    pagPrimera : function () {
        this.state.pagina = 1
        this.cargarServicios(this.state.hrefPrimera)
    },
    pagSiguiente : function () {
        if (this.state.pagina < this.state.paginasTotales) {
            this.state.pagina++
        }
        this.cargarServicios(this.state.hrefSiguiente)
    },
    pagAnterior : function () {
        if (this.state.pagina > 1) {
            this.state.pagina--
        }
        this.cargarServicios(this.state.hrefAnterior)
    },
    pagUltima : function () {
        this.state.pagina = this.state.paginasTotales
        this.cargarServicios(this.state.hrefUltima)
    },
    getPaginasTotales : function () {
        return this.state.hrefUltima.split("=")[1]
    },
    irAPagina : function (pagina) {
        var n = parseInt(pagina)
        if (n > 0 && n <= this.state.paginasTotales) {
            this.cargarServicios(this.state.queryString + n)
            this.state.pagina = pagina
        } else {
            alert("Introduzca una página entre: 1 y " + this.state.paginasTotales)
        }
    },
    render: function () {
        var servicios = []
        var paginacion = []
        var pagApi

        for (var i = 0; i < this.state.servicios.length - 3; i++) {
            /* GUARDANDO LA PAGINACIÓN */
            pagApi = this.state.servicios[1].paginacion
            for (var ii = 0; ii < pagApi.length; ii++) {
                var tupla = pagApi[ii]
                switch (tupla.rel) {
                    case 'primera' : this.state.hrefPrimera = tupla.href
                        break
                    case 'anterior' : this.state.hrefAnterior = tupla.href
                        break
                    case 'siguiente' : this.state.hrefSiguiente = tupla.href
                        break
                    case 'ultima' : this.state.hrefUltima = tupla.href
                        break
                }
            }
            this.state.paginasTotales = this.getPaginasTotales()
            /* FIN GUARDANDO LA PAGINACIÓN */

            var lista = this.state.servicios[0]
            this.lista = lista

            for (var j = 0; j < lista.length; j++) {
                var actual = lista[j]
                var elemento

                var art = this.getArtista(actual.artista)
                var artid = this.getArtistaId(actual.artista)

                if (this.state.detalleServicio == j) {
                    elemento = <DetallesServicio pos={j}
                                         nombre={actual.nombre}
                                         descripcion={actual.descripcion}
                                         artista={art}
                                         handleOcultarDetalles={this.ocultarDetalles}/>
                } else if (this.state.editarServicio == j) {
                    elemento = <EditarServicio pos={j}
                                         id={actual.id}
                                         nombre={actual.nombre}
                                         descripcion={actual.descripcion}
                                         artista={art}
                                         artistaid={artid}
                                         handleEditarServicio={this.editarServicio}
                                         handleOcultarEditarServicio={this.ocultarEditarServicio}/>
                } else {
                    elemento = <Servicio pos={j}
                                     id={actual.id}
                                     nombre={actual.nombre}
                                     handleVerDetalles={this.verDetalles}
                                     handleVerEditarServicio={this.verEditarServicio}
                                     handleEliminarServicio={this.eliminarServicio}/>
                }
                servicios.push(elemento)
            }
        }

        paginacion.push(<Paginacion 
                            tamPagina={this.state.tamPagina}
                            handlesetTamPagina={this.setTamPagina}
                            handlePagPrimera={this.pagPrimera}
                            handlePagAnterior={this.pagAnterior}
                            handlePagSiguiente={this.pagSiguiente}
                            handlePagUltima={this.pagUltima}
                            paginasTotales={this.state.paginasTotales}
                            paginaActual={this.state.pagina}
                            handleIrAPagina={this.irAPagina}
                        />)

       return <div className="col-xs-8 servicios">
                  <h3>Tu lista de peticiones</h3>
                  {servicios}
                  {paginacion}
               </div>
    }
})

