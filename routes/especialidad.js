/*
* ADRIAN JOSE GOSALVEZ MACIA
* 48620879-Y
*/
var express 	= require('express')
var especialidad 	= express.Router()
module.exports 	= especialidad

require('../helpers/tools')()
var connection = connect()

/**
  * Obtiene listado completo de especialidad
  * @name Obtener_todos_los_especialidad
  * @example GET /especialidad
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
especialidad.get('/', checkAuth, function(pet, resp){ 
	totalRows('especialidad', function(err, total){
        if (err) {
            console.log("ERROR : ",err); 
            resp.status(500).send("Error en el servidor").end()           
		} else {            
            var query = 'SELECT * FROM especialidad'
			var subQuery = paginacionQuery(pet, total)
			console.log(query + subQuery)
			connection.query(query + subQuery, function(err, rows, fields){
				if (!err) {
					if (rows.length == 0) {
						resp.status(404).send("Objeto no encontrado").end()
					} else {
						var hm = hipermedia("especialidad", null, total)
						var pages = paginacion("especialidad", pet.query.pagina, total)
						var basic = {"status-basic:":"authenticated"}
						var res = [rows, pages, hm, basic] 
						resp.status(200).send(res).end()
					}
				} else {
					console.log("Error 500")
					resp.status(500).send("Server error").end()
				}
			}) 
        }
	})
})

/**
  * Obtiene un único especialidad
  * @name Obtener_un_especialidad
  * @param {id} Identificador de especialidad
  * @example GET /especialidad/1
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
especialidad.get('/:id', checkAuth, function(pet, resp){
	var id = parseInt(pet.params.id)
	var query = 'SELECT * FROM especialidad WHERE id = ' + id
	connection.query(query, function(err, rows, fields){
		if (isNaN(id)) {
			resp.status(400).send("Objeto no encontrado").end();
		} else {
			if (!err) {
				if (rows.length == 0) {
					resp.status(404).send("Objeto no encontrado").end()
				} else {
					var hm = hipermedia("especialidadid", id)
					var basic = {"status-basic:":"authenticated"}
					var res = [rows, hm, basic] 
					console.log("Get especialidad/" + id + " ok!")
					resp.status(200).send(res).end()
				}
			} else {
				console.log("Error en la consulta")
				resp.status(500).send("Error interno").end()
			}
		}
	}) 
})

/**
  * Graba un especialidad nuevo
  * @name Nuevo_especialidad
  * @example POST /especialidad/ {'nombre'}
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
especialidad.post('/', checkAuth, function(pet, resp){
	var nuevo 		= pet.body
	var nombre 		= nuevo.nombre
	var descripcion	= nuevo.descripcion

	var query = 'INSERT INTO especialidad (nombre, descripcion) VALUES ("' + nombre + '", "' + descripcion + '")'
	console.log(query)
	connection.query(query, function(err, rows, fields){
		if (!err) {
			if (rows["affectedRows"] == 1) {
				console.log("OK: " + query)
				resp.header('Location','especialidad/' + rows["insertId"]) 
				resp.status(200)
				resp.send({"message":"Especialidad creada", "status":"ok"})
				resp.end()
			} else {
				console.log("ERROR: " + query)
				resp.status(200)
				resp.send({"message":"Especialidad no creada, revise la petición", "status":"error"})
				resp.end()
			}
		} else {
				console.log("ERROR2: " + query)
				resp.status(200)
				resp.send({"message":"Especialidad existente", "status":"error"})
				resp.end()
		}
	}) 
})

/**
  * Editar un especialidad existente
  * @name Editar_especialidad
  * @param {id} Identificador de especialidad
  * @example PUT /especialidad/1 {'magia'}
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
especialidad.put('/:id', checkAuth, function(pet, resp){
	var nuevo 		= pet.body
	var id 			= pet.params.id
	var nombre 		= nuevo.nombre
	var descripcion	= nuevo.descripcion

	var query = 'UPDATE especialidad SET nombre = "' + nombre + '", descripcion = "' + descripcion + '" WHERE id = ' + id
	connection.query(query, function(err, rows, fields){
		if (isNaN(id)) {
			console.log("id incorrecto")
			resp.status(400);
			resp.send("request error")
			resp.end();
		} else {
			if (!err) {
				if (rows["affectedRows"] == 1) {
					console.log("OK: " + query)
					resp.status(200)
					resp.send({"status":"ok","message":"Especialidad editada"})
					resp.end()
				} else {
					console.log("ERROR: " + query)
					resp.status(400)
					resp.send({"message":"request error"})
					resp.end()
				}
			} else {
				console.log("ERROR2: " + query)
				console.log(query)
				resp.status(200)
				resp.send({"status":"error","message":"Especialidad duplicada"})
				resp.end()
			}
		}
	}) 
})

/**
  * Borrar un especialidad nuevo
  * @name Borrar_un_especialidad
  * @param {id} Identificador de especialidad
  * @example DELETE /especialidad/1
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
especialidad.delete('/:id', checkAuth, function(pet, resp){
	var id = parseInt(pet.params.id)
	var query = 'DELETE FROM especialidad WHERE id = ' + id
	connection.query(query, function(err, rows, fields){
		console.log(rows)
		if (isNaN(id)) {
			resp.status(400).send("request error").end()
		} else {
			if (!err) {
				resp.status(200).send({"status":"ok","message":"Especialidad eliminada"}).end()
			} else {
				console.log("Error en la consulta")
				resp.status(500).send({"status":"error","message":"Especialidad no eliminada"}).end()
			}
		}
	}) 
})