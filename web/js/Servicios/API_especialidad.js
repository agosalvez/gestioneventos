
module.exports  = {
    API_URL_ESPECIALIDAD : 'http://localhost:3000/especialidad',

    obtenerEspecialidad: function () {
        return fetch(this.API_URL_ESPECIALIDAD, {
                  headers: { 'Authorization':'Basic dXN1YXJpbzoxMjM0NTY=' }
              }).then(function(response) {
                if (response.ok)
                    return response.json()
              })
    },
    obtenerUnaEspecialidad: function (id) {
        return fetch(this.API_URL_ESPECIALIDAD + '/' + id, {
                  headers: { 'Authorization':'Basic dXN1YXJpbzoxMjM0NTY=' }
              }).then(function(response) {
                if (response.ok)
                    return response.json()
              })
    },
    nuevaEspecialidad: function (especialidad) {
        return fetch(this.API_URL_ESPECIALIDAD, {
                   method: 'POST',
                   headers: { 'Content-type':'application/json', 'Authorization':'Basic dXN1YXJpbzoxMjM0NTY=' },
                   body: JSON.stringify(especialidad)
               }).then(function (respuesta) {
                   if (respuesta.ok)
                      return respuesta.json()
               })
    },
    editarEspecialidad: function (id, especialidad) {
        return fetch(this.API_URL_ESPECIALIDAD + '/' + id, {
                   method: 'PUT',
                   headers: { 'Content-type':'application/json', 'Authorization':'Basic dXN1YXJpbzoxMjM0NTY=' },
                   body: JSON.stringify(especialidad)
               }).then(function (respuesta) {
                   if (respuesta.ok)
                      return respuesta.json()
               })
    },
    eliminarEspecialidad: function (id) {
        return fetch(this.API_URL_ESPECIALIDAD + '/' + id, {
                   method: 'DELETE',
                   headers: {'Authorization':'Basic dXN1YXJpbzoxMjM0NTY='}
               }).then(function (respuesta) {
                   if (respuesta.ok)
                      return respuesta.json()
               })
    }
}
