var React = require('react')
var Artista = require('../Artistas/Artistas')
var DetallesArtista = require('../Artistas/DetallesArtistas')
var Paginacion = require('../Servicios/Paginacion')
var API_artista = require('../servicios/API_artista')
//var EventBus = require('./servicios/EventBus')

module.exports = React.createClass({
    componentDidMount: function () {
        //escuchamos el evento 'nuevoCliente' en el bus de eventos
        //si se recibe el evento hay que añadir el item a la lista
        //EventBus.eventEmitter.addListener('nuevoCliente', this.addItem)
        //le pedimos los items al API
        this.cargarArtistasList(this.state.paginaInicial)
    },
    getInitialState : function () {
      return {servicios : [],
                artistas : [],
                paginaInicial : "/artistas/?pagina=1", 
                queryString : "/artistas/?pagina=", 
                pagina : 1,
                tamPagina : 3}
    },
    verDetalles : function (i) {
        this.setState({detalleArtista: i})
    },
    ocultarDetalles : function () {
        this.setState({detalleArtista: undefined})
    },
    cargarArtistasList: function (direccion) {
        API_artista.obtenerArtistas(direccion, this.state.tamPagina)
            .then(datos => {
                this.setState({artistas: datos})
            })
    },
    verEditarServicio: function(i) {
        this.setState({editarServicio: i})
    },
    ocultarEditarServicio: function(i) {
        this.setState({editarServicio: undefined})
    },
    setTamPagina : function (tamPagina) {
        this.state.tamPagina = tamPagina
        this.pagPrimera()
    },
    pagPrimera : function () {
        this.state.pagina = 1
        this.cargarArtistasList(this.state.hrefPrimera)
    },
    pagSiguiente : function () {
        if (this.state.pagina < this.state.paginasTotales) {
            this.state.pagina++
        }
        this.cargarArtistasList(this.state.hrefSiguiente)
    },
    pagAnterior : function () {
        if (this.state.pagina > 1) {
            this.state.pagina--
        }
        this.cargarArtistasList(this.state.hrefAnterior)
    },
    pagUltima : function () {
        this.state.pagina = this.state.paginasTotales
        this.cargarArtistasList(this.state.hrefUltima)
    },
    getPaginasTotales : function () {
        return this.state.hrefUltima.split("=")[1]
    },
    irAPagina : function (pagina) {
        var n = parseInt(pagina)
        if (n > 0 && n <= this.state.paginasTotales) {
            this.cargarArtistasList(this.state.queryString + n)
        } else {
            alert("Introduzca una página entre: 1 y " + this.state.paginasTotales)
        }
    },
    render: function () {
        var paginacion = []
        var pagApi

        for (var i = 0; i < this.state.artistas.length - 3; i++) {
            /* GUARDANDO LA PAGINACIÓN */
            pagApi = this.state.artistas[1].paginacion
            
            for (var ii = 0; ii < pagApi.length; ii++) {
                var tupla = pagApi[ii]
                switch (tupla.rel) {
                    case 'primera' : this.state.hrefPrimera = tupla.href
                        break
                    case 'anterior' : this.state.hrefAnterior = tupla.href
                        break
                    case 'siguiente' : this.state.hrefSiguiente = tupla.href
                        break
                    case 'ultima' : this.state.hrefUltima = tupla.href
                        break
                }
            }
            this.state.paginasTotales = this.getPaginasTotales()
            /* FIN GUARDANDO LA PAGINACIÓN */

            var lista = this.state.artistas[0]
            this.lista = lista
            var artistas = []
            for (var j = 0; j < lista.length; j++) {
                var actual = lista[j]
                var elemento
                if (this.state.detalleArtista == j) {
                    elemento = <DetallesArtista pos={j}
                                     id={actual.id}
                                     nombreArt={actual.nombreArt}
                                     nombre={actual.nombre}
                                     apellidos={actual.apellidos}
                                     especialidad={actual.especialidad}
                                     telefono={actual.telefono}
                                     handleOcultarDetalles={this.ocultarDetalles}/>    
                } else {
                    elemento = <Artista pos={j}
                                     id={actual.id}
                                     nombreArt={actual.nombreArt}
                                     handleVerDetalles={this.verDetalles}/>    
                }
                artistas.push(elemento)
            }
        }
        paginacion.push(<Paginacion 
                            tamPagina={this.state.tamPagina}
                            handlesetTamPagina={this.setTamPagina}
                            handlePagPrimera={this.pagPrimera}
                            handlePagAnterior={this.pagAnterior}
                            handlePagSiguiente={this.pagSiguiente}
                            handlePagUltima={this.pagUltima}
                            paginasTotales={this.state.paginasTotales}
                            paginaActual={this.state.pagina}
                            handleIrAPagina={this.irAPagina}
                        />)

        return <div className="col-xs-8 artistas">
                  <h3>Tu lista de artistas</h3>
                  {artistas}
                  {paginacion}
               </div>
    }
})

