var React = require('react')

module.exports = React.createClass({
    componentDidMount: function () {
        $(".tamPagina").val(this.props.tamPagina)
    },
    setTamPagina: function () {
        this.props.handlesetTamPagina(this.tamPagina.value)
    },
    primera: function () {
        this.props.handlePagPrimera()
    },
    siguiente: function () {
        this.props.handlePagSiguiente()
    },
    anterior: function () {
        this.props.handlePagAnterior()
    },
    ultima: function () {
        this.props.handlePagUltima()
    },
    irAPagina: function () {
        this.props.handleIrAPagina(this.numPagina.value)
        document.getElementById("irAPagina").value = ""
    },
    render: function () {
    	return <div>
                    <a className="btn btn-sm btn-danger" href="javascript:;" onClick={this.primera}>Primera</a>
                    <a className="btn btn-sm btn-success" href="javascript:;" onClick={this.anterior}>Anterior</a>
                    <a className="btn btn-sm btn-success" href="javascript:;" onClick={this.siguiente}>Siguiente</a>
                    <a className="btn btn-sm btn-danger" href="javascript:;" onClick={this.ultima}>Última</a>
                    <br />
                    <label>Página actual: {this.props.paginaActual} Páginas totales: {this.props.paginasTotales}</label>
                    <br />
                    <label>Mostrar&nbsp;</label><input className="tamPagina" type="text" ref={(campo)=>{this.tamPagina=campo}} size="1"/><label>&nbsp;filas&nbsp;</label>
                    <a className="btn btn-sm btn-info" href="javascript:;" onClick={this.setTamPagina}>Mostrar</a>
                    <br />
                    <label>Ir a la página&nbsp;</label><input id="irAPagina" type="text" ref={(campo)=>{this.numPagina=campo}} size="2"/><label>&nbsp;</label>
                    <a className="btn btn-sm btn-info" href="javascript:;" onClick={this.irAPagina}>Ir</a>
				</div>
    }
})