var React = require('react')
var API_cliente = require('../servicios/API_cliente')
//var EventBus = require('./servicios/EventBus')

var NuevoItemComponente = React.createClass({
    clickAdd: function () {
       var nuevoCliente = {
           nombre: this.nombre.value,
           apellidos: this.apellidos.value,
           dni: this.dni.value,
           direccion: this.direccion.value,
           telefono: this.telefono.value,
           dob: this.dob.value,
           password: this.password.value
       }
       API_cliente.addCliente(nuevoCliente).then(function(datos){
            if (datos.response == "ok") {
//           EventBus.eventEmitter.emitEvent('nuevoCliente', [nuevoCliente])
              alert("Se ha registrado correctamente")
              $('#miFormRegistro').trigger("reset");
            } else {
              console.log("registro error")
            }
       })
    },
    render: function () {
        return <div className="col-xs-12 registroCliente">
            <h4>Registrese en la aplicación y comience a pedir presupuestos</h4>
            <form action="" method="" id="miFormRegistro">
              <input type="text" placeholder="Dni" ref={(campo)=>{this.dni=campo}}/> <br/>
              <input type="password" placeholder="Password" ref={(campo)=>{this.password=campo}}/> <br/>
              <input type="text" placeholder="Nombre" ref={(campo)=>{this.nombre=campo}}/> <br/>
              <input type="text" placeholder="Apellidos" ref={(campo)=>{this.apellidos=campo}}/> <br/>
              <input type="text" placeholder="Dirección" ref={(campo)=>{this.direccion=campo}}/> <br/>
              <input type="text" placeholder="Teléfono" ref={(campo)=>{this.telefono=campo}}/> <br/>
              <input type="text" placeholder="F.Nacim (AAAA-MM-DD)" ref={(campo)=>{this.dob=campo}}/> <br/>
              <button onClick={this.clickAdd}>Registrarse</button>
            </form> 
        </div>
    }
})
module.exports = NuevoItemComponente
