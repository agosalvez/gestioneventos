/*
* ADRIAN GOSALVEZ MACIA
*/
//vars
var express 		= require('express')
var app 			= express()
var bp 				= require('body-parser')
var db 				= require('mysql')
var jsesc 			= require('jsesc')
var clientes 		= require('./routes/clientes')
var servicios 		= require('./routes/servicios')
var artistas 		= require('./routes/artistas')
var presupuestos 	= require('./routes/presupuestos')
var comentarios 	= require('./routes/comentarios')
var agenda 			= require('./routes/agenda')
var pagos 			= require('./routes/pagos')
var token			= require('./routes/token')
var especialidad	= require('./routes/especialidad')
var database		= require('./database/database')

//uses
app.use(bp.json())
app.use('/clientes', clientes)
app.use('/servicios', servicios)
app.use('/artistas', artistas)
app.use('/presupuestos', presupuestos)
app.use('/comentarios', comentarios)
app.use('/agenda', agenda)
app.use('/pagos', pagos)
app.use('/token', token)
app.use('/especialidad', especialidad)
app.use('/database', database)
app.use('/web', express.static('web'))


app.get('*', function(pet, resp){
	resp.status(400).send('Empty request!!')
})

app.listen(3000,function(){
	console.log('Marchando el servidor en localhost:3000')
})
