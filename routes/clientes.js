/*
* ADRIAN JOSE GOSALVEZ MACIA
* 48620879-Y
*/
var express 	= require('express')
var clientes 	= express.Router()
module.exports 	= clientes

require('../helpers/tools')()
var connection = connect()

/**
  * Obtiene listado completo de clientes
  * @name Obtener_todos_los_clientes
  * @example GET /clientes
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
clientes.get('/', checkAuth, function(pet, resp){ 
	totalRows('cliente', function(err, total){
        if (err) {
            console.log("ERROR : ",err); 
            resp.status(500).send("Error en el servidor").end()           
		} else {            
            var query = 'SELECT * FROM cliente'
			var subQuery = paginacionQuery(pet, total)
			console.log(query + subQuery)
			connection.query(query + subQuery, function(err, rows, fields){
				if (!err) {
					if (rows.length == 0) {
						resp.status(404).send("Objeto no encontrado").end()
					} else {
						var hm = hipermedia("clientes", null, total)
						var pages = paginacion("clientes", pet.query.pagina, total)
						var basic = {"status-basic:":"authenticated"}
						var res = [rows, pages, hm, basic] 
						resp.status(200).send(res).end()
					}
				} else {
					console.log("Error 500")
					resp.status(500).send("Server error").end()
				}
			}) 
        }
	})
})

/**
  * Obtiene un único cliente
  * @name Obtener_un_cliente
  * @param {id} Identificador de cliente
  * @example GET /clientes/1
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
clientes.get('/:id', checkAuth, function(pet, resp){
	var idCliente = parseInt(pet.params.id)
	var query = 'SELECT * FROM cliente WHERE id = ' + idCliente
	connection.query(query, function(err, rows, fields){
		if (isNaN(idCliente)) {
			resp.status(400).send("Objeto no encontrado").end();
		} else {
			if (!err) {
				if (rows.length == 0) {
					resp.status(404).send("Objeto no encontrado").end()
				} else {
					var hm = hipermedia("clientesid", idCliente)
					var basic = {"status-basic:":"authenticated"}
					var res = [rows, hm, basic] 
					console.log("Get clientes/" + idCliente + " ok!")
					resp.status(200).send(res).end()
				}
			} else {
				console.log("Error en la consulta")
				resp.status(500).send("Error interno").end()
			}
		}
	}) 
})

/**
  * Graba un cliente nuevo
  * @name Nuevo_cliente
  * @example POST /clientes/ {'nombre':'adrian','apellidos':'gosalvez','dni':'12345678A','direccion':'mi calle,3', 'telefono':'965656565', 'dob':'18-02-1990', 'famNumerosa':0, 'password':'123'}
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
clientes.post('/', checkAuth, function(pet, resp){
	var nuevo 		= pet.body
	var nombre 		= nuevo.nombre
	var apellidos 	= nuevo.apellidos
	var dni 		= nuevo.dni
	var direccion 	= nuevo.direccion
	var telefono 	= nuevo.telefono
	var dob 		= nuevo.dob
	var famNumerosa = nuevo.famNumerosa
	var password 	= nuevo.password

	//var query = 'INSERT INTO cliente (nombre, apellidos, dni, direccion, telefono, dob, famNumerosa, password) VALUES ("' + nombre + '", "' + apellidos + '", "' + dni + '", "' + direccion + '", "' + telefono + '", "' + dob + '", ' + famNumerosa + ', MD5("' + password + '"))'
	//var query = 'INSERT INTO cliente (nombre, apellidos) VALUES ("' + nombre + '", "' + apellidos + '")'
	var query = 'INSERT INTO cliente (nombre, apellidos, dni, direccion, telefono, dob, password) VALUES ("' + nombre + '", "' + apellidos + '", "' + dni + '", "' + direccion + '", "' + telefono + '", "' + dob + '", MD5("' + password + '"))'
	console.log(query)
	connection.query(query, function(err, rows, fields){
		if (!err) {
			if (rows["affectedRows"] == 1) {
				console.log("OK: " + query)
				resp.header('Location','clientes/' + rows["insertId"]) 
				resp.status(201)
				resp.send({"message":"Usuario creado", "response":"ok"})
				resp.end()
			} else {
				console.log("ERROR: " + query)
				resp.status(400)
				resp.send({"message":"Usuario no creado, revise la petición", "response":"error"})
				resp.end()
			}
		} else {
				console.log("ERROR2: " + query)
				resp.status(400)
				resp.send({"message":"Usuario existente", "response":"error"})
				resp.end()
		}
	}) 
})

/**
  * Editar un cliente existente
  * @name Editar_cliente
  * @param {id} Identificador de cliente
  * @example PUT /clientes/1 {'adrian','gosalvez','12345678A','mi calle,3', '965656565', '18-02-1990', 0, '123'}
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
clientes.put('/:id', checkAuth, function(pet, resp){
	var nuevo 		= pet.body
	var idCliente 	= pet.params.id
	var nombre 		= nuevo.nombre
	var apellidos 	= nuevo.apellidos
	var dni 		= nuevo.dni
	var direccion 	= nuevo.direccion
	var telefono 	= nuevo.telefono
	var dob 		= nuevo.dob
	var famNumerosa = nuevo.famNumerosa
	var password 	= nuevo.password

	var query = 'UPDATE cliente SET nombre = "' + nombre + '", apellidos = "' + apellidos + '", dni = "' + dni + '", direccion = "' + direccion + '", telefono = "' + telefono + '", dob = "' + dob + '", famNumerosa = ' + famNumerosa + ', password = MD5("' + password + '") WHERE id = ' + idCliente
	connection.query(query, function(err, rows, fields){
		if (isNaN(idCliente)) {
			console.log("id incorrecto")
			resp.status(400);
			resp.send("request error")
			resp.end();
		} else {
			if (!err) {
				if (rows["affectedRows"] == 1) {
					console.log("OK: " + query)
					resp.status(200)
					resp.send("ok")
					resp.end()
				} else {
					console.log("ERROR: " + query)
					resp.status(400)
					resp.send("request error")
					resp.end()
				}
			} else {
				console.log("ERROR2: " + query)
				console.log(query)
				resp.status(500)
				resp.send("server error")
				resp.end()
			}
		}
	}) 
})

/**
  * Borrar un cliente nuevo
  * @name Borrar_un_cliente
  * @param {id} Identificador de cliente
  * @example DELETE /clientes/1
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
clientes.delete('/:id', checkAuth, function(pet, resp){
	var idCliente = parseInt(pet.params.id)
	var query = 'DELETE FROM cliente WHERE id = ' + idCliente
	connection.query(query, function(err, rows, fields){
		console.log(rows)
		if (isNaN(idCliente)) {
			resp.status(400).send("request error").end()
		} else {
			if (!err) {
				resp.status(200).send("ok").end()
			} else {
				console.log("Error en la consulta")
				resp.status(500).send("server error").end()
			}
		}
	}) 
})