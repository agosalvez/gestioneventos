var React = require('react')
var API_servicio = require('../servicios/API_servicio')
//var EventBus = require('./servicios/EventBus')

var NuevoItemComponente = React.createClass({
    nuevoServicio: function () {
       var servicio = {
           nombre: this.nombre.value,
           descripcion: this.descripcion.value,
           artista: this.artista.value,
       }
       API_servicio.nuevoServicio(servicio).then(function(response){
            if(response.status == "ok"){
                mensaje(response.message, "success")
                $("#nombreNew").val("")
                $("#descripcionNew").val("")
                $("#artistaNew").val("")
            } else {
                mensaje(response.message, "danger")
            }
       })
    },
    render: function () {
        return <div className="col-xs-8 nuevoServicio">
                  <h3>Registra una nueva petición</h3>
                  <div id="miFormRegistro">
                    <input id="nombreNew" type="text" placeholder="Nombre" ref={(campo)=>{this.nombre=campo}}/><br/>
                    <input id="descripcionNew" type="text" placeholder="Descripción" ref={(campo)=>{this.descripcion=campo}}/><br/>
                    <input id="artistaNew" type="text" placeholder="Id del artista" ref={(campo)=>{this.artista=campo}}/><br/>
                    <button onClick={this.nuevoServicio}>Nueva petición</button>
                  </div> 
              </div>
    }
})
module.exports = NuevoItemComponente
