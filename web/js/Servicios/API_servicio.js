
module.exports  = {
    API_URL : 'http://localhost:3000',
    API_URL_SERVICIOS : 'http://localhost:3000/servicios/',

    obtenerServicios: function (direccion, total) {
        return fetch(this.API_URL + direccion + '&total=' + total, {
                    headers: {'Authorization':'Basic dXN1YXJpbzoxMjM0NTY='}
                }).then(function(response) {
                  if (response.ok) {
                      return response.json()
                    }
                })
    },
    nuevoServicio: function (servicio) {
        return fetch(this.API_URL_SERVICIOS, {
                   method: 'POST',
                   headers: { 'Content-type':'application/json', 'Authorization':'Basic dXN1YXJpbzoxMjM0NTY=' },
                   body: JSON.stringify(servicio)
               }).then(function (respuesta) {
                   if (respuesta.ok)
                      return respuesta.json()
               })
    },
    editarServicio: function (id, servicio) {
        return fetch(this.API_URL_SERVICIOS + id, {
                   method: 'PUT',
                   headers: { 'Content-type':'application/json', 'Authorization':'Basic dXN1YXJpbzoxMjM0NTY=' },
                   body: JSON.stringify(servicio)
               }).then(function (respuesta) {
                   if (respuesta.ok)
                      return respuesta.json()
               })
    },
    eliminarServicio: function (id) {
        return fetch(this.API_URL_SERVICIOS + id, {
                   method: 'DELETE',
                   headers: {'Authorization':'Basic dXN1YXJpbzoxMjM0NTY='}
               }).then(function (respuesta) {
                   if (respuesta.ok)
                      return respuesta.json()
               })
    }
}
