/*
* ADRIAN JOSE GOSALVEZ MACIA
* 48620879-Y
*/
var express 	= require('express')
var agenda 		= express.Router()
module.exports 	= agenda

require('../helpers/tools')()
var connection = connect()

/**
  * Obtiene listado completo de la agenda
  * @name Obtener_todos_los_dias_de_la_agenda
  * @example GET /agenda
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
agenda.get('/', checkAuth, function(pet, resp){ 
	totalRows('agenda', function(err, total){
        if (err) {
            console.log("ERROR : ",err); 
            resp.status(500).send("Error en el servidor").end()           
		} else {            
            var query = 'SELECT * FROM agenda'
			var subQuery = paginacionQuery(pet, total)
			console.log(query + subQuery)
			connection.query(query + subQuery, function(err, rows, fields){
				if (!err) {
					if (rows.length == 0) {
						resp.status(404).send("Objeto no encontrado").end()
					} else {
						var hm = hipermedia("agenda", null, total)
						var pages = paginacion("agenda", pet.query.pagina, total)
						var basic = {"status-basic:":"authenticated"}
						var res = [rows, pages, hm, basic] 
						resp.status(200).send(res).end()
					}
				} else {
					console.log("Error 500")
					resp.status(500).send("Server error").end()
				}
			}) 
        }
	})
})

/**
  * Obtiene un único dia de la agenda
  * @name Obtener_un_dia_de_la_agenda
  * @param {año} Año de la fecha a buscar
  * @param {mes} Mes de la fecha a buscar
  * @param {dia} Día de la fecha a buscar
  * @example GET /agenda/2016/05/10
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
agenda.get('/:anyo/:mes/:dia', checkAuth, function(pet, resp){
	var anyo 	= pet.params.anyo
	var mes 	= pet.params.mes
	var dia 	= pet.params.dia
	var fecha 	= anyo + '-' + mes + '-' + dia
	var query 	= 'SELECT * FROM agenda WHERE fecha LIKE "%' + fecha + '%"'
	connection.query(query, function(err, rows, fields){
		if (!err) {
			if (rows.length == 0) {
				resp.status(404)
				resp.send("Objeto no encontrado")
				resp.end()
			} else {
				var hm = hipermedia("agenda")
				var basic = {"status-basic:":"authenticated"}
				var res = [rows, hm, basic] 
				console.log("Get agenda ok!")
				resp.status(200).send(res).end()
			}
		} else {
			console.log("Error en la consulta")
			resp.status(404)
			resp.send("Objeto no encontrado")
			resp.end()
		}
	}) 
})
