/*
* ADRIAN JOSE GOSALVEZ MACIA
* 48620879-Y
*/
var express 	= require('express')
var servicios 	= express.Router()
module.exports 	= servicios

require('../helpers/tools')()
var connection = connect()

/**
  * Obtiene listado completo de servicios
  * @name Obtener_todos_los_servicios
  * @example GET /servicios
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
servicios.get('/', checkAuth, function(pet, resp){ 
	totalRows('servicio', function(err, total){
        if (err) {
            console.log("ERROR : ",err); 
            resp.status(500).send("Error en el servidor").end()           
		} else {            
            var query = 'SELECT * FROM servicio'
			var subQuery = paginacionQuery(pet, total, pet.query.total)
			console.log(query + subQuery)
			connection.query(query + subQuery, function(err, rows, fields){
				if (!err) {
					if (rows.length == 0) {
						resp.status(404).send("Objeto no encontrado").end()
					} else {
						var hm = hipermedia("servicios", null, total)
						var pages = paginacion("servicios", pet.query.pagina, total, pet.query.total)
						var basic = {"status-basic:":"authenticated"}
						var res = [rows, pages, hm, basic] 
						resp.status(200).send(res).end()
					}
				} else {
					console.log("Error 500")
					resp.status(500).send("Server error").end()
				}
			}) 
        }
	})
})

/**
  * Obtiene un único servicio
  * @name Obtener_un_servicio
  * @param {id} Identificador de servicio
  * @example GET /servicios/1
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
servicios.get('/:id', checkAuth, function(pet, resp){
	var idServicio = parseInt(pet.params.id)
	var query = 'SELECT * FROM servicio WHERE id = ' + idServicio
	connection.query(query, function(err, rows, fields){
		if (isNaN(idServicio)) {
			resp.status(400);
			resp.send("Objeto no encontrado")
			resp.end();
		} else {
			if (!err) {
				if (rows.length == 0) {
					resp.status(404).send("Objeto no encontrado").end()
				} else {
					var hm = hipermedia("serviciosid", idServicio)
					var basic = {"status-basic:":"authenticated"}
					var res = [rows, hm, basic] 
					console.log(rows)
					resp.status(200).send(res).end()
				}
			} else {
				console.log("Error en la consulta")
				resp.status(404).send("Objeto no encontrado").end()
			}
		}
	}) 
})

/**
  * Graba un cliente servicio
  * @name Nuevo_servicio
  * @example POST /servicios/ {'nombre':'adrian','descripcion':'Esto es una descripción','artista':1}
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
servicios.post('/', checkAuth, function(pet, resp){
	var nuevo 			= pet.body
	var nombre 			= nuevo.nombre
	var descripcion 	= nuevo.descripcion
	var artista 		= nuevo.artista

	var query = 'INSERT INTO servicio (nombre, descripcion, artista) VALUES ("' + nombre + '", "' + descripcion + '", ' + artista + ')'
	connection.query(query, function(err, rows, fields){
		if (!err) {
			if (rows["affectedRows"] == 1) {
				console.log("OK: " + query)
				resp.header('Location','servicios/' + rows["insertId"]) 
				resp.status(201)
				resp.send({status:"ok",message:"Petición creado correctamente"})
				resp.end()
			} else {
				console.log("ERROR: " + query)
				resp.status(400)
				resp.send("request error")
				resp.end()
			}
		} else {
				console.log("ERROR2: " + query)
				resp.status(500)
				resp.send("server error")
				resp.end()
		}
	}) 
})

/**
  * Editar un servicio existente
  * @name Editar_servicio
  * @param {id} Identificador de servicio
  * @example PUT /servicios/1 {'nombre':'Nuevo nombre de servicio','descripcion':'Nueva descripción de servicio', 'artista':2}
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
servicios.put('/:id', checkAuth, function(pet, resp){
	var nuevo 			= pet.body
	var idServicio		= pet.params.id
	var nombre 			= nuevo.nombre
	var descripcion 	= nuevo.descripcion
	var artista 		= nuevo.artista

	var query = 'UPDATE servicio SET nombre = "' + nombre + '", descripcion = "' + descripcion + '", artista = "' + artista + '" WHERE id = ' + idServicio
	connection.query(query, function(err, rows, fields){
		if (isNaN(idServicio)) {
			console.log("id incorrecto")
			resp.status(400);
			resp.send("request error")
			resp.end();
		} else {
			if (!err) {
				if (rows["affectedRows"] == 1) {
					console.log("OK: " + query)
					resp.status(200)
					resp.send({status:"ok",message:"Petición editada correctamente"})
					resp.end()
				} else {
					console.log("ERROR: " + query)
					resp.status(400)
					resp.send("request error")
					resp.end()
				}
			} else {
				console.log("ERROR2: " + query)
				console.log(query)
				resp.status(500)
				resp.send("server error")
				resp.end()
			}
		}
	}) 
})

/**
  * Borrar un servicio nuevo
  * @name Borrar_un_servicio
  * @param {id} Identificador de servicio
  * @example DELETE /servicios/1
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
servicios.delete('/:id', checkAuth, function(pet, resp){
	var idServicio = parseInt(pet.params.id)
	var query = 'DELETE FROM servicio WHERE id = ' + idServicio
	connection.query(query, function(err, rows, fields){
		console.log(rows)
		if (isNaN(idServicio)) {
			resp.status(400);
			resp.send("request error")
			resp.end();
		} else {
			if (!err) {
				resp.status(200).send({status:"ok",message:"Petición eliminada correctamente"}).end()
			} else {
				console.log("Error en la consulta")
				resp.status(500)
				resp.send("server error")
				resp.end()
			}
		}
	}) 
})
