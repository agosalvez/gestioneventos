
module.exports  = {
    API_URL : 'http://localhost:3000/',

    obtenerClientes: function () {
        return fetch(this.API_URL + 'clientes', {
                  headers: { 'Authorization':'Basic dXN1YXJpbzoxMjM0NTY=' }
              }).then(function(response) {
                if (response.ok)
                    return response.json()
              })
    },
    addCliente: function (cliente) {
        return fetch(this.API_URL + 'clientes', {
                   method: 'POST',
                   headers: { 'Content-type':'application/json', 'Authorization':'Basic dXN1YXJpbzoxMjM0NTY=' },
                   body: JSON.stringify(cliente)
               }).then(function (respuesta) {
                   if (respuesta.ok)
                      return respuesta.json()
               })
    },
    login: function (cliente) {
        return fetch(this.API_URL + 'token', {
                   method: 'POST',
                   headers: { 'Content-type':'application/json' },
                   body: JSON.stringify(cliente)
               }).then(function(response) {
                if (response.ok)
                    return response.json()
            })
    }

}
