var React = require('react')

module.exports = React.createClass({
    componentDidMount: function () {
        document.getElementById("nombreServicio").value = this.props.nombre
        document.getElementById("artistaServicioId").value = this.props.artistaid
        document.getElementById("descripcionServicio").value = this.props.descripcion
    },
    editarServicio: function () {
      var servicio = {
        nombre : this.nombre.value,
        descripcion : this.descripcion.value,
        artista : this.artistaid.value
      }
      this.props.handleEditarServicio(this.props.id, servicio)
    },
    ocultarEditarServicio: function () {
      this.props.handleOcultarEditarServicio()
    },
    render: function () {
        return <div className="detalleServicio">
                  <h4 className="nombre">{this.props.nombre}</h4>
                  <table>
                    <tbody>
                      <tr>
                        <td><label>Nombre</label></td>
                        <td><input id="nombreServicio" type="text" placeholder="Nombre" ref={(campo)=>{this.nombre=campo}}/></td>
                      </tr>
                      <tr>
                        <td><label>Descripción</label></td>
                        <td><input id="descripcionServicio" type="text" placeholder="Artista" ref={(campo)=>{this.descripcion=campo}}/></td>
                      </tr>
                      <tr>
                        <td><label>Id de artista</label></td>
                        <td><input id="artistaServicioId" type="text" placeholder="Descripción" ref={(campo)=>{this.artistaid=campo}}/></td>
                      </tr>
                      <tr>
                        <td><label>Nombre artístico</label></td>
                        <td><label>{this.props.artista}</label></td>
                      </tr>
                      <tr>
                        <td><a className="btn btn-success" href="javascript:;" onClick={this.editarServicio}>Editar servicio</a></td>
                        <td><a className="btn btn-info" href="javascript:;" onClick={this.ocultarEditarServicio}>Cerrar</a></td>
                      </tr>
                    </tbody>
                  </table>
            </div>
    }
})

