var React = require('react')

module.exports = React.createClass({
	verDetalles: function () {
      this.props.handleVerDetalles(this.props.pos)
    },
    verEditarServicio: function () {
      this.props.handleVerEditarServicio(this.props.pos)
    },
    editarServicio: function () {
      this.props.handleEditarServicio(this.props.pos)
    },
    eliminarServicio: function () {
    	this.props.handleEliminarServicio(this.props.id)
    },
    render: function () {

    	return <div>
                    <table>
                        <tbody>
                            <tr><td className="nameTable">{this.props.nombre}</td>
                				<td><a className="btn btn-sm btn-success" href="javascript:;" onClick={this.verDetalles}>Detalles</a></td>
                				<td>&nbsp;</td>
                				<td><a className="btn btn-sm btn-warning" href="javascript:;" onClick={this.verEditarServicio}>Editar</a></td>
                				<td>&nbsp;</td>
                				<td><a className="btn btn-sm btn-danger" href="javascript:;" onClick={this.eliminarServicio}>Eliminar</a></td>
        				    </tr>
                        </tbody>
                    </table>
                </div>
    }
})