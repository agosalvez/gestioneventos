var React = require('react')
var ReactDOM = require('react-dom')

if ( localStorage.getItem('login') != undefined && localStorage.getItem('password') != undefined ) {

	var Servicios = require('./Servicios/ListaServicios')
	ReactDOM.render(<Servicios/>, document.getElementById('componenteServicios'))

	var Artistas = require('./Artistas/ListaArtistas')
	ReactDOM.render(<Artistas/>, document.getElementById('componenteArtistas'))

	var NuevoServicio = require('./Servicios/NuevosServicios')
	ReactDOM.render(<NuevoServicio/>, document.getElementById('componenteNuevoServicio'))

	//manejador de eventos para cuando se carga la página
	//le pedimos la lista de items al servidor y la pintamos en el HTML
	document.addEventListener('DOMContentLoaded', function(){
		cargarEspecialidad()
	})

} else {

	var Login = require('./Clientes/Login')
	ReactDOM.render(<Login/>, document.getElementById('componenteLogin'))

	var NuevoCliente = require('./Clientes/NuevoCliente')
	ReactDOM.render(<NuevoCliente/>, document.getElementById('componenteNuevoCliente'))
}

var AcercaDe = require('./Acercade/Acercade')
ReactDOM.render(<AcercaDe/>, document.getElementById('componenteAcercaDe'))



/* COMIENZO DE LA PARTE JAVASCRIPT NATIVO + HANDLEBARS */

var API_especialidad = require('./servicios/API_especialidad.js')
var handlebars = require('handlebars')

/* TEMPLATES */
var templateItem = `
	<tr id="edit{{id}}">
		<td class="nameTable"><span id="{{id}}"><strong>{{nombre}}</strong></span></td>
		<td><a class="btn btn-info btn-sm" id="enlace_{{id}}" href="javascript:verDetalles({{id}})">Detalles</a></td>
		<td><a class="btn btn-warning btn-sm" id="edit_enlace_{{id}}" href="javascript:verEditar({{id}})">Editar</a></td>
		<td><a class="btn btn-danger btn-sm" id="elim_enlace_{{id}}" href="javascript:eliminar({{id}})">Borrar</a></td>
	</tr>
`
var templateLista = `
<h3>Lista de las especialidades solicitadas</h3>
<h5>JS native + handlebars</h5>
<table>
	<tbody>
		{{#.}}
			${templateItem}
		{{/.}}
	</tbody>
</table>
`

var templateDetalles = `
  <span class="nameTable2" id="detalles_{{id}}">
    {{descripcion}}
  </span>
`
var templateEditar = `
	<div>
		<label>Nombre&nbsp;</label><input type="text" name="nombre" id="nombreEsp" value="{{nombre}}"/>&nbsp;&nbsp;
		<label>Descripción&nbsp;</label><input type="text" name="descripcion" id="descripcionEsp" value="{{descripcion}}"/>&nbsp;&nbsp;
		<a class="btn btn-warning btn-sm" id="edit_enlace_{{id}}" href="javascript:editar({{id}})">Guardar edición</a>
	</div>
`

var templateNueva = `
	<div id="formNew">
		<label>Nombre&nbsp;</label><input type="text" name="nombre" id="newNombreEsp" placeholder="Nombre" value=""/>&nbsp;&nbsp;
		<label>Descripción&nbsp;<input type="text" name="descripcion" id="newDescripcionEsp" placeholder="Descripción" value=""/>&nbsp;&nbsp;
	</div>
`
var templateAux = `
	<div id="newEsp">
		<a class="btn btn-success btn-sm" id="new_enlace" href="javascript:verNueva()">Nueva especialidad</a>
	</div>
`
/* FIN TEMPLATES */

//Compilamos las plantillas handlebars. Esto genera funciones a las que llamaremos luego
var tmpl_lista_compilada = handlebars.compile(templateLista)
var tmpl_item_compilada = handlebars.compile(templateItem)
var tmpl_detalles_compilada = handlebars.compile(templateDetalles)
var tmpl_editar_compilada = handlebars.compile(templateEditar)
var tmpl_nueva_compilada = handlebars.compile(templateNueva)
var tmpl_aux_compilada = handlebars.compile(templateAux)




/* Función que carga las especialidades de los artistas*/
function cargarEspecialidad() {
	API_especialidad.obtenerEspecialidad().then(function(datos) {
		//mezclamos los datos con el HTML de la plantilla para obtener el HTML resultado
		var listaHTML = tmpl_lista_compilada(datos[0])
		//insertamos el HTML en la página
		var obj = document.getElementById("miComponenteHandlebars")
		var aux = tmpl_aux_compilada()
		obj.innerHTML = listaHTML
		obj.insertAdjacentHTML('beforeend', aux)
	})
}

/* Llamada cuando pulsamos en un enlace "Detalles" */
function verDetalles(id) {
	API_especialidad.obtenerUnaEspecialidad(id).then(function(item){
		var aux = item[0][0]
		//creamos un objeto JS con los datos de los detalles a mostrar
		var datos = {id: aux.id, nombre: aux.nombre, descripcion: aux.descripcion}
		//lo fusionamos con la plantilla handlebars
		var datosHTML = tmpl_detalles_compilada(datos)
		//metemos el HTML resultante en la página
	    //aprovechamos que hemos hecho que el item con un id determinado
	    //esté en el HTML en un div con el mismo id
		var divItem = document.getElementById(id)
		divItem.insertAdjacentHTML('beforeend', datosHTML)
		//TEDIOSO: ahora hay que cambiar el enlace "ver detalles" por uno "ocultar"
		//hemos hecho que el HTML del enlace tenga un id con "enlace_" y el id del item
		var enlaceDetalles = document.getElementById('enlace_'+id)
		//Cambiamos a dónde apunta el enlace
		enlaceDetalles.href = 'javascript:ocultarDetalles('+ id +')'
		//cambiamos el texto del enlace
		enlaceDetalles.innerHTML = 'Cerrar'
	})
}

//llamada cuando pulsamos en un enlace "Ocultar Detalles"
function ocultarDetalles(id) {
	//forma sencilla de eliminar un fragmento HTML, asignarle la cadena vacía
	//usamos outerHTML porque incluye la propia etiqueta, innerHTML sería solo el contenido
	document.getElementById('detalles_'+id).outerHTML = ''
	//TEDIOSO: volvemos a poner el enlace en modo "mostrar detalles"
	document.getElementById('enlace_'+id).href = 'javascript:verDetalles('+id+')'
	document.getElementById('enlace_'+id).innerHTML = 'Detalles'
}

/* Llamada cuando pulsamos en un enlace "Editar" */
function verEditar(id) {
	API_especialidad.obtenerUnaEspecialidad(id).then(function(item){
		var aux = item[0][0]
		var datos = {id: aux.id, nombre: aux.nombre, descripcion: aux.descripcion}
		var datosHTML = tmpl_editar_compilada(datos)
		var divItem = document.getElementById("edit"+id)
		divItem.insertAdjacentHTML('beforeend', datosHTML)
		//TEDIOSO: ahora hay que cambiar el enlace "ver detalles" por uno "ocultar"
		//hemos hecho que el HTML del enlace tenga un id con "enlace_" y el id del item
		var enlaceEditar = document.getElementById('edit_enlace_'+id)
		enlaceEditar.href = 'javascript:;'
		enlaceEditar.innerHTML = 'Editar'
	})
}
/* Llamada cuando pulsamos en el botón para editar el ítem en cuestión */
function editar(id) {
	var nombre = document.getElementById("nombreEsp").value
	var descripcion = document.getElementById("descripcionEsp").value
	var item = {"id":id, "nombre":nombre, "descripcion":descripcion}
	API_especialidad.editarEspecialidad(id, item).then(function(response) {
		cargarEspecialidad()
		if(response.status == "ok"){
			mensaje(response.message, "success")
		} else {
			mensaje(response.message, "danger")
		}
	})
}

/* Llamada cuando pulsamos en un enlace "Nueva especialidad" */
function verNueva() {
	var datosHTML = tmpl_nueva_compilada()
	var divItem = document.getElementById("newEsp")
	divItem.insertAdjacentHTML('afterbegin', datosHTML)
	//TEDIOSO: ahora hay que cambiar el enlace "ver detalles" por uno "ocultar"
	//hemos hecho que el HTML del enlace tenga un id con "enlace_" y el id del item
	var enlaceNueva = document.getElementById('new_enlace')
	enlaceNueva.href = 'javascript:nueva()'
	enlaceNueva.className = 'btn btn-success btn-sm'
	enlaceNueva.innerHTML = 'Crear especialidad'
}

/* Llamada cuando pulsamos en el botón para añadir un nuevo ítem */
function nueva(id) {
	var nombre = document.getElementById("newNombreEsp").value
	var descripcion = document.getElementById("newDescripcionEsp").value
	if (nombre != '' && descripcion != '') {
		var item = {"id":id, "nombre":nombre, "descripcion":descripcion}
		API_especialidad.nuevaEspecialidad(item).then(function(response) {
			cargarEspecialidad()
			ocultarNueva()
			if(response.status == "ok"){
				mensaje(response.message, "success")
			} else {
				mensaje(response.message, "danger")
			}
		})
	} else {
		mensaje("Rellene los campos", "danger")
	}
}

/* Llamada cuando creamos una nueva especialidad, para borrar el child del form con .remove() */
function ocultarNueva() {
	document.getElementById("formNew").remove()
	var enlaceNueva = document.getElementById('new_enlace')
	enlaceNueva.href = 'javascript:verNueva()'
	enlaceNueva.className = 'btn btn-success btn-sm'
	enlaceNueva.innerHTML = 'Nueva especialidad'

}

/* Llamada cuando pulsamos en el enlace de Eliminar */
function eliminar(id) {
	API_especialidad.eliminarEspecialidad(id).then(function(response) {
		cargarEspecialidad()
		if(response.status == "ok"){
			mensaje(response.message, "success")
		} else {
			mensaje(response.message, "danger")
		}
	})
}

/* Función auxiliar para la gestión de mensajes aprovechando la librería bootstrap */
function mensaje(msg, type) {
	$("#mensaje").removeClass("alert-warning alert-success alert-danger alert-info").addClass("alert alert-"+type).html(msg).fadeIn("slow").delay(1500).fadeOut("slow")
}

//IMPORTANTE: para que desde la página se pueda llamar a la función,
// la guardamos en el ámbito global (window). Si no, no será visible,
//porque el código del main.js no es visible directamente para el HTML, sino el bundle.js
window.verDetalles = verDetalles
window.ocultarDetalles = ocultarDetalles
window.verNueva = verNueva
window.nueva = nueva
window.verEditar = verEditar
window.editar = editar
window.eliminar = eliminar
window.ocultarNueva = ocultarNueva
window.mensaje = mensaje
