/*
* ADRIAN JOSE GOSALVEZ MACIA
* 48620879-Y
*/
var express 	= require('express')
var presupuestos 	= express.Router()
module.exports 	= presupuestos

require('../helpers/tools')()
var connection = connect()

/**
  * Obtiene listado completo de presupuestos
  * @name Obtener_todos_los_presupuestos
  * @example GET /presupuestos
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
presupuestos.get('/', checkAuth, function(pet, resp){
	totalRows('presupuesto', function(err, total){
        if (err) {
            console.log("ERROR : ",err); 
            resp.status(500).send("Error en el servidor").end()           
		} else {            
            var query = 'SELECT * FROM presupuesto'
			var subQuery = paginacionQuery(pet, total)
			console.log(query + subQuery)
			connection.query(query + subQuery, function(err, rows, fields){
				if (!err) {
					if (rows.length == 0) {
						resp.status(404).send("Objeto no encontrado").end()
					} else {
						var hm = hipermedia("presupuestos", null, total)
						var pages = paginacion("presupuestos", pet.query.pagina, total)
						var basic = {"status-basic:":"authenticated"}
						var res = [rows, pages, hm, basic] 
						resp.status(200).send(res).end()
					}
				} else {
					console.log("Error 500")
					resp.status(500).send("Server error").end()
				}
			}) 
        }
	})
})

/**
  * Obtiene un único presupuesto
  * @name Obtener_un_presupuesto
  * @param {id} Identificador de presupuesto
  * @example GET /presupuestos/1
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
presupuestos.get('/:id', checkAuth, function(pet, resp){
	var idPresupuesto = parseInt(pet.params.id)
	var query = 'SELECT * FROM presupuesto WHERE id = ' + idPresupuesto
	connection.query(query, function(err, rows, fields){
		if (isNaN(idPresupuesto)) {
			resp.status(400);
			resp.send("Objeto no encontrado")
			resp.end();
		} else {
			if (!err) {
				if (rows.length == 0) {
					resp.status(404)
					resp.send("Objeto no encontrado")
					resp.end()
				} else {
					var hm = hipermedia("presupuestosid", idPresupuesto)
					var basic = {"status-basic:":"authenticated"}
					var res = [rows, hm, basic] 
					console.log("Get presupuestos/ ok!")
					resp.status(200).send(res).end()
				}
			} else {
				console.log("Error en la consulta")
				resp.status(404)
				resp.send("Objeto no encontrado")
				resp.end()
			}
		}
	}) 
})

/**
  * Graba un cliente presupuesto
  * @name Nuevo_presupuesto
  * @example POST /presupuestos {'idCliente':1,'idServicio':2,'tipo':'Boda','fechaEvento':'2016-12-12', 'variosAnfitriones':1, 'cantidad':15, 'lugar':'Restaurante', 'musica':0, 'tiempo':45, 'comentario':''}
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
presupuestos.post('/', checkAuth, function(pet, resp){
	var nuevo 				= pet.body
	var idCliente 			= nuevo.idCliente
	var idServicio 			= nuevo.idServicio
	var tipo 				= nuevo.tipo
	var fechaEvento 		= nuevo.fechaEvento
	var variosAnfitriones 	= nuevo.variosAnfitriones
	var cantidad 			= nuevo.cantidad
	var lugar 				= nuevo.lugar
	var musica 				= nuevo.musica
	var tiempo 				= nuevo.tiempo
	var comentario 			= nuevo.comentario

	var query = 'INSERT INTO presupuesto (idCliente, idServicio, tipo, fechaEvento, variosAnfitriones, cantidad, lugar, musica, tiempo, comentario) VALUES	(' + idCliente + ', ' + idServicio + ', "' + tipo + '", "' + fechaEvento + '", ' + variosAnfitriones + ', ' + cantidad + ', "' + lugar + '", ' + musica + ', ' + tiempo + ', "' + comentario + '")'
		connection.query(query, function(err, rows, fields){
		if (!err) {
			if (rows["affectedRows"] == 1) {
				console.log("OK: " + query)
				resp.header('Location','presupuestos/' + rows["insertId"]) 
				resp.status(201)
				resp.send("ok")
				resp.end()
			} else {
				console.log("ERROR: " + query)
				resp.status(400)
				resp.send("request error")
				resp.end()
			}
		} else {
				console.log("ERROR2: " + query)
				resp.status(500)
				resp.send("server error")
				resp.end()
		}
	}) 
})
