var React = require('react')
var ReactDOM = require('react-dom')
var handlebars = require('handlebars')
var API_cliente = require('../servicios/API_cliente')
var API_especialidad = require('../servicios/API_especialidad.js')
//var EventBus = require('../servicios/EventBus')

module.exports = React.createClass({
    login: function () {
        var login = {
           dni: this.dni.value,
           password: this.password.value
       }
       API_cliente.login(login).then(function(response){
            // borro los campos del form
            $('#miFormRegistro').trigger("reset");
            if(response.response == "ok"){
                // hago login en localstorage
                localStorage.setItem('login', login.dni);
                localStorage.setItem('password', login.password);
                localStorage.setItem("auth", 'Basic ' + new Buffer("usuario:123456").toString('base64'));
                mensaje(response.message, "success")
                
                $("#usernameLogin").html("User: " + login.dni)
                $("#componenteLogin").hide()
                $("#componenteNuevoCliente").hide()

                var Servicios = require('../Servicios/ListaServicios')
                ReactDOM.render(<Servicios/>, document.getElementById('componenteServicios'))

                var Artistas = require('../Artistas/ListaArtistas')
                ReactDOM.render(<Artistas/>, document.getElementById('componenteArtistas'))

                var NuevoServicio = require('../Servicios/NuevosServicios')
                ReactDOM.render(<NuevoServicio/>, document.getElementById('componenteNuevoServicio'))

                /* CARGO CODIGO JAVASCRIPT NATIVO + HANDLEBARS */
                var templateItem = `
                    <tr id="edit{{id}}">
                        <td class="nameTable"><span id="{{id}}"><strong>{{nombre}}</strong></span></td>
                        <td><a class="btn btn-info btn-sm" id="enlace_{{id}}" href="javascript:verDetalles({{id}})">Detalles</a></td>
                        <td><a class="btn btn-warning btn-sm" id="edit_enlace_{{id}}" href="javascript:verEditar({{id}})">Editar</a></td>
                        <td><a class="btn btn-danger btn-sm" id="elim_enlace_{{id}}" href="javascript:eliminar({{id}})">Borrar</a></td>
                    </tr>
                `
                var templateAux = `
                    <div id="newEsp">
                        <a class="btn btn-success btn-sm" id="new_enlace" href="javascript:verNueva()">Nueva especialidad</a>
                    </div>
                `
                var templateLista = `
                <h3>Lista de las especialidades solicitadas</h3>
                <h5>JS native + handlebars</h5>
                <table>
                    <tbody>
                        {{#.}}
                            ${templateItem}
                        {{/.}}
                    </tbody>
                </table>    
                ` 
                var tmpl_lista_compilada = handlebars.compile(templateLista)
                var tmpl_aux_compilada = handlebars.compile(templateAux)
                API_especialidad.obtenerEspecialidad().then(function(datos) {
                    //mezclamos los datos con el HTML de la plantilla para obtener el HTML resultado
                    var listaHTML = tmpl_lista_compilada(datos[0])
                    //insertamos el HTML en la página
                    var obj = document.getElementById("miComponenteHandlebars")
                    var aux = tmpl_aux_compilada()
                    obj.innerHTML = listaHTML
                    obj.insertAdjacentHTML('beforeend', aux)    
                })  
            } else {
                mensaje(response.message, "danger")
            }
       })
    },
    render: function () {        
        return <div className="col-xs-12 loginCliente">
                    <h4>Inicia sesión</h4>
                    <label>Datos de acceso de test para Otto:</label><br />
                    <label>Usuario: usuario</label><br />
                    <label>Password: 123456</label><br />
                    <input type="text" placeholder="Dni" ref={(campo)=>{this.dni=campo}}/> <br/>
                    <input type="password" placeholder="Password" ref={(campo)=>{this.password=campo}}/> <br/>
                    <button onClick={this.login}>Iniciar sesión</button>
                </div>
    }
})
