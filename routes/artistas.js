/*
* ADRIAN JOSE GOSALVEZ MACIA
* 48620879-Y
*/
var express 	= require('express')
var artistas 	= express.Router()
module.exports 	= artistas

require('../helpers/tools')()
var connection = connect()

/**
  * Obtiene listado completo de artistas
  * @name Obtener_todos_los_artistas
  * @example GET /artistas
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
artistas.get('/', checkAuth, function(pet, resp){ 
	totalRows('artista', function(err, total){
        if (err) {
            console.log("ERROR : ",err); 
            resp.status(500).send("Error en el servidor").end()           
		} else {            
            var query = 'SELECT * FROM artista'
			var subQuery = paginacionQuery(pet, total, pet.query.total)
			console.log(query + subQuery)
			connection.query(query + subQuery, function(err, rows, fields){
				if (!err) {
					if (rows.length == 0) {
						resp.status(404).send("Objeto no encontrado").end()
					} else {
						var hm = hipermedia("artistas", null, total)
						var pages = paginacion("artistas", pet.query.pagina, total, pet.query.total)
						var basic = {"status-basic:":"authenticated"}
						var res = [rows, pages, hm, basic] 
						resp.status(200).send(res).end()
					}
				} else {
					console.log("Error 500")
					resp.status(500).send("Server error").end()
				}
			}) 
        }
	})
})

/**
  * Obtiene un único artista
  * @name Obtener_un_artista
  * @param {id} Identificador de artista
  * @example GET /artistas/1
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
artistas.get('/:id', checkAuth, function(pet, resp){
	var idArtista = parseInt(pet.params.id)
	var query = 'SELECT * FROM artista WHERE id = ' + idArtista
	connection.query(query, function(err, rows, fields){
		if (isNaN(idArtista)) {
			resp.status(400);
			resp.send("Objeto no encontrado")
			resp.end();
		} else {
			if (!err) {
				if (rows.length == 0) {
					resp.status(404)
					resp.send("Objeto no encontrado")
					resp.end()
				} else {
					var hm = hipermedia("artistasid", idArtista)
					var basic = {"status-basic:":"authenticated"}
					var res = [rows, hm, basic] 
					console.log("Get artistas/ ok!")
					resp.status(200).send(res).end()
				}
			} else {
				console.log("Error en la consulta")
				resp.status(404)
				resp.send("Objeto no encontrado")
				resp.end()
			}
		}
	}) 
})

/**
  * Graba un artista nuevo
  * @name Nuevo_artista
  * @example POST /artistas {'nombreArt':'Mago Juan',nombre':'Juan','apellidos':'Tamariz','direccion':'mi calle, 23', 'telefono':'965656565', 'dob':'18-02-1990', 'especialidad':'magia con cartas'}
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
artistas.post('/', checkAuth, function(pet, resp){
	var nuevo 			= pet.body
	var nombreArt 		= nuevo.nombreArt
	var nombre 			= nuevo.nombre
	var apellidos 		= nuevo.apellidos
	var direccion 		= nuevo.direccion
	var telefono 		= nuevo.telefono
	var dob 			= nuevo.dob
	var especialidad 	= nuevo.especialidad

	var query = 'INSERT INTO artista (nombreArt, nombre, apellidos, dob, especialidad, direccion, telefono) VALUES ("' + nombreArt + '", "' + nombre + '", "' + apellidos + '", "' + dob + '", "' + especialidad + '", "' + direccion + '", "' + telefono + '")'
	connection.query(query, function(err, rows, fields){
		if (!err) {
			if (rows["affectedRows"] == 1) {
				console.log("OK: " + query)
				resp.header('Location','artistas/' + rows["insertId"]) 
				resp.status(201)
				resp.send("ok")
				resp.end()
			} else {
				console.log("ERROR: " + query)
				resp.status(400)
				resp.send("request error")
				resp.end()
			}
		} else {
				console.log("ERROR2: " + query)
				resp.status(500)
				resp.send("server error")
				resp.end()
		}
	}) 
})

/**
  * Graba un artista nuevo
  * @name Nuevo_artista
  * @param {id} Identificador de artista
  * @example PUT /artistas/1 {'nombreArt':'Mago Juan',nombre':'Juan','apellidos':'Tamariz','direccion':'mi calle, 23', 'telefono':'965656565', 'dob':'18-02-1990', 'especialidad':'magia con cartas'}
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
artistas.put('/:id', checkAuth, function(pet, resp){
	var nuevo 			= pet.body
	var idArtista		= pet.params.id
	var nombreArt 		= nuevo.nombreArt
	var nombre 			= nuevo.nombre
	var apellidos 		= nuevo.apellidos
	var direccion 		= nuevo.direccion
	var telefono 		= nuevo.telefono
	var dob 			= nuevo.dob
	var especialidad 	= nuevo.especialidad

	var query = 'UPDATE artista SET nombreArt = "' + nombreArt + '", nombre = "' + nombre + '", apellidos = "' + apellidos + '", dob = "' + dob + '", especialidad = "' + especialidad + '", direccion = "' + direccion + '", telefono = "' + telefono + '" WHERE id = ' + idArtista
	connection.query(query, function(err, rows, fields){
		if (isNaN(idArtista)) {
			console.log("id incorrecto")
			resp.status(400);
			resp.send("request error")
			resp.end();
		} else {
			if (!err) {
				if (rows["affectedRows"] == 1) {
					console.log("OK: " + query)
					resp.status(200)
					resp.send("ok")
					resp.end()
				} else {
					console.log("ERROR: " + query)
					resp.status(400)
					resp.send("request error")
					resp.end()
				}
			} else {
				console.log("ERROR2: " + query)
				console.log(query)
				resp.status(500)
				resp.send("server error")
				resp.end()
			}
		}
	}) 
})

/**
  * Borrar un artista nuevo
  * @name Borrar_un_artista
  * @param {id} Identificador de artista
  * @example DELETE /artistas/1
  * @example Authentication BASIC: {"username" : "usuario", "password" : "123456"}
  * @return {respuesta petición}
  */
artistas.delete('/:id', checkAuth, function(pet, resp){
	var idArtista = parseInt(pet.params.id)
	var query = 'DELETE FROM artista WHERE id = ' + idArtista
	connection.query(query, function(err, rows, fields){
		console.log(rows)
		if (isNaN(idArtista)) {
			resp.status(400);
			resp.send("request error")
			resp.end();
		} else {
			if (!err) {
				resp.status(200)
				resp.send("ok")
				resp.end()
			} else {
				console.log("Error en la consulta")
				resp.status(500)
				resp.send("server error")
				resp.end()
			}
		}
	}) 
})
