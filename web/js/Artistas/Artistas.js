var React = require('react')

module.exports = React.createClass({
	verDetalles: function () {
      this.props.handleVerDetalles(this.props.pos)
    },
    render: function () {
        return <div>
                    <table>
                        <tbody>
                    		<tr>
                    			<td className="nameTable">{this.props.id}.- {this.props.nombreArt}</td>
                    			<td><a className="btn btn-sm btn-success" href="javascript:;" onClick={this.verDetalles}>Detalles</a></td>
                			</tr>
                        </tbody>
                    </table>
		      </div>
    }
})